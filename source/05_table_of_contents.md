<!-- \pagenumbering{gobble} -->
<!-- https://tex.stackexchange.com/a/173423 -->
\clearpage                      
\pagestyle{empty}
\fancypagestyle{plain}

\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\tableofcontents
\thispagestyle{empty}


<!-- \thispagestyle{empty}
\tableofcontents -->

\newpage