<!-- This page is for an official declaration. -->

\newpage

\addcontentsline{toc}{chapter}{Declaration}

\vspace*{\fill}
\noindent
\textit{
I, Kai Huang confirm that the work presented in this thesis is my own. Where information has been derived from other sources, I confirm that this has been indicated in the thesis.
}
\vspace*{\fill}
\thispagestyle{empty}
\newpage
